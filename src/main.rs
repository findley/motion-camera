mod cli;
mod detect;
mod handler;
mod multipart;
mod server;
mod video_encoder;
mod yuv;

use anyhow::Result;
use clap::{Parser, ValueEnum};
use image::{ImageBuffer, Luma};
use std::env;
use std::path::PathBuf;
use std::time;
use v4l::buffer::Type;
use v4l::io::traits::CaptureStream;
use v4l::video::Capture;
use v4l::{prelude::*, Format, FourCC};

use video_encoder::VideoEncoder;

use crate::handler::DetectionHandler;

type DetectionFrame = ImageBuffer<Luma<u8>, Vec<u8>>;

#[allow(dead_code)]
enum State {
    Warmup(time::SystemTime),
    Searching,
    Detected(time::SystemTime, time::SystemTime),
    /// Detection is disabled, we should close camera stream to save power
    Idle,
}

impl State {
    pub fn should_detect(&self) -> bool {
        match self {
            Self::Searching | Self::Detected(_, _) => true,
            _ => false,
        }
    }
}

fn main() -> Result<()> {
    dotenv::dotenv().ok();
    let cli = cli::Cli::parse();
    let level = match cli.verbose {
        0 => "info",
        1 => "debug",
        _ => "trace",
    };
    env_logger::Builder::from_env(env_logger::Env::default().default_filter_or(level)).init();

    let cam_path = choose_cam_path(cli.cam);
    let (mut dev, fmt) = prepare_device(cam_path);

    let sim_thresh = cli
        .similarity_thresh
        .unwrap_or(cli.strategy.default_thresh());
    let mut stream =
        MmapStream::with_buffers(&mut dev, Type::VideoCapture, 4).unwrap_or_else(|err| {
            log::error!("Failed to create buffer stream: {}", err);
            std::process::exit(1);
        });
    let mut prev_frame: Option<DetectionFrame> = None;
    let mut last_frame_time = time::Instant::now();
    log::info!(
        "Using {} detection strategy",
        cli.strategy.to_possible_value().unwrap().get_name()
    );

    let mut encoder = VideoEncoder::new(fmt.width, fmt.height);
    let mut handlers: Vec<Box<dyn DetectionHandler>> = Vec::new();
    handlers.push(Box::new(handler::echo::EchoHandler {}));
    match (
        env::var("MOTION_CAM_DISCORD_BOT_TOKEN"),
        env::var("MOTION_CAM_DICORD_DM_USER"),
    ) {
        (Ok(token), Ok(user)) => {
            handlers.push(Box::new(handler::discord::DiscordHandler::new(token, user)));
        }
        _ => {}
    }
    log::info!(
        "Registered handlers: {}",
        handlers
            .iter()
            .map(|h| h.name())
            .collect::<Vec<String>>()
            .join(", ")
    );

    let (detecting_tx, detecting_rx) = crossbeam_channel::bounded::<bool>(1);
    std::thread::spawn(|| {
        server::start(detecting_tx).unwrap();
    });

    let mut state = State::Warmup(time::SystemTime::now());

    loop {
        crossbeam_channel::select! {
          recv(detecting_rx) -> detecting => {
              state = if detecting? {
                  State::Searching
              } else {
                  State::Idle
              };
              log::info!("Detecting has been toggled to {}", detecting?);
          },
          default => {
              let (buf, meta) = stream.next().unwrap();
              match state {
                  State::Warmup(t0) => {
                      if t0.elapsed()?.as_millis() > 2000 {
                          state = State::Searching;
                      }
                  }
                  State::Idle => {}
                  State::Detected(initial, last) => {
                      let detect_expired =
                          initial.elapsed()?.as_secs() > 30 || last.elapsed()?.as_secs() > 5;
                      if detect_expired {
                          log::info!("DETECTION END");
                          state = State::Searching
                      }
                      if let Some(dir) = &cli.out_dir {
                          encoder.encode_frame(buf, meta)?;
                          if detect_expired {
                              let ts = initial.duration_since(time::UNIX_EPOCH)?.as_millis();
                              let file_path = dir.join(format!("./detection-{}.mp4", ts));
                              encoder.save_file(&file_path);
                              for h in handlers.iter_mut() {
                                  h.on_clip_ready(initial, file_path.clone());
                              }
                          }
                      }
                  }
                  State::Searching => {}
              }

              if last_frame_time.elapsed() < time::Duration::from_millis(cli.interval) {
                  continue;
              }

              let t0 = time::SystemTime::now();
              let frame = make_detection_frame(buf, &fmt);
              if let Some(prev) = prev_frame.as_ref() {
                  let sim = cli.strategy.similarity(prev, &frame);
                  log::trace!("similarity is: {}", sim);
                  if sim < sim_thresh && state.should_detect() {
                      handle_motion(
                          prev,
                          &frame,
                          &mut state,
                          t0,
                          sim,
                          &mut handlers,
                          &mut encoder,
                          &cli.location,
                      )?;
                  }
              }

              prev_frame = Some(frame);
              last_frame_time = time::Instant::now();
              log::debug!(
                  "Processed frame for motion detection in {}ms",
                  t0.elapsed()?.as_millis()
              );
          }
        };
    }
}

fn handle_motion(
    prev_frame: &DetectionFrame,
    frame: &DetectionFrame,
    state: &mut State,
    ts: time::SystemTime,
    sim: f64,
    handlers: &mut [Box<dyn DetectionHandler>],
    encoder: &mut VideoEncoder,
    location: &str,
) -> Result<()> {
    *state = match state {
        State::Searching => {
            log::info!("DETECTION START");
            for h in handlers.iter_mut() {
                h.on_detect(ts, sim, location);
            }
            encoder.start_recording()?;
            State::Detected(ts, ts)
        }
        State::Detected(inital, _last) => {
            log::debug!("DETECTION EXTENDED");
            State::Detected(*inital, ts)
        }
        State::Idle | State::Warmup(_) => unreachable!(),
    };

    if env::var("MOTION_CAM_WRITE_DEBUG_IMAGES").is_ok() {
        log::debug!("saving debug images");
        let ts = time::SystemTime::now()
            .duration_since(time::UNIX_EPOCH)
            .unwrap_or_default()
            .as_millis();
        prev_frame.save(format!("detect-{}.a.jpg", ts))?;
        frame.save(format!("detect-{}.b.jpg", ts))?;
    }

    Ok(())
}

fn make_detection_frame(buf: &[u8], fmt: &Format) -> DetectionFrame {
    // FIXME: Handle converting various formats to lum
    let lum_data = yuyv_to_lum(buf);
    ImageBuffer::<Luma<u8>, Vec<u8>>::from_raw(fmt.width, fmt.height, lum_data).unwrap()
}

fn prepare_device(cam_path: PathBuf) -> (Device, Format) {
    log::info!(
        "Opening camera \"{}\"",
        cam_path.to_str().unwrap_or_default()
    );
    let dev = Device::with_path(cam_path).unwrap_or_else(|err| {
        log::error!("Could not open camera: {}", err.to_string());
        std::process::exit(1);
    });

    log::info!("Checking camera settings");
    let formats = dev.enum_formats().unwrap_or_else(|err| {
        log::error!(
            "Could not enumerate supported video formats: {}",
            err.to_string()
        );
        std::process::exit(1);
    });

    let mut fmt = dev.format().unwrap_or_else(|err| {
        log::error!("Could not get camera settings: {}", err.to_string());
        std::process::exit(1);
    });

    log::info!(
        "Supported formats: {}",
        formats
            .iter()
            .map(|f| format!("{} ({})", f.fourcc, f.description))
            .collect::<Vec<String>>()
            .join(", ")
    );

    // FIXME: This is not safe, check formats first
    fmt.width = 640;
    fmt.height = 480;
    fmt.fourcc = FourCC::new(b"YUYV");
    dev.set_format(&fmt).unwrap_or_else(|err| {
        log::error!("Could not configure camera settings: {}", err.to_string());
        std::process::exit(1);
    });

    (dev, fmt)
}

fn list_cam_paths() -> Vec<PathBuf> {
    let v4l_cams_by_id = std::fs::read_dir("/dev/v4l/by-id")
        .map(|res| res.filter_map(|entry| entry.map(|path| path.path()).ok()))
        .ok();

    if let Some(cams) = v4l_cams_by_id {
        let mut cam_paths = cams.collect::<Vec<_>>();
        if !cam_paths.is_empty() {
            cam_paths.sort();
            return cam_paths;
        }
    }

    let v4l_cams_by_path = std::fs::read_dir("/dev/v4l/by-path")
        .map(|res| res.filter_map(|entry| entry.map(|path| path.path()).ok()))
        .ok();

    match v4l_cams_by_path {
        Some(cams) => {
            let mut result = cams.collect::<Vec<_>>();
            result.sort();
            result
        }
        None => Vec::new(),
    }
}

fn choose_cam_path(user_cam_path: Option<PathBuf>) -> PathBuf {
    match user_cam_path {
        Some(path) => {
            if !path.exists() {
                log::error!(
                    "The camera path \"{}\" does not exist! Exiting...",
                    path.to_str().unwrap_or_default()
                );
                std::process::exit(1);
            }
            path
        }
        None => {
            let cams = list_cam_paths();
            log::info!(
                "Available cameras are: {}",
                cams.iter()
                    .map(|p| p.to_str().unwrap())
                    .collect::<Vec<&str>>()
                    .join(", ")
            );
            if cams.is_empty() {
                log::error!("No cameras available! Exiting...");
                std::process::exit(1);
            }
            cams[0].clone()
        }
    }
}

fn yuyv_to_lum(yuyv_buffer: &[u8]) -> Vec<u8> {
    let mut gray_buffer = Vec::with_capacity((yuyv_buffer.len() / 2) as usize);

    for chunk in yuyv_buffer.chunks(4) {
        gray_buffer.push(chunk[0]);
        gray_buffer.push(chunk[2]);
    }

    gray_buffer
}
