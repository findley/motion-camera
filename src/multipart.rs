use std::fs::File;
use std::io;
use std::io::prelude::*;
use std::path::Path;

use mime::Mime;

const BOUNDARY_UNIQUE: &'static str = "cdm6ZiO5gyM6pcWActonTMSXL6t7z";

pub struct MultipartBuilder {
    inner: Vec<u8>,
    data_written: bool,
}

impl MultipartBuilder {
    pub fn new() -> Self {
        Self {
            inner: Vec::new(),
            data_written: false,
        }
    }

    pub fn text(mut self, name: &str, text: &str) -> io::Result<Self> {
        self.write_field_headers(name, None, None)?;
        self.inner.write_all(text.as_bytes())?;
        Ok(self)
    }

    pub fn file<P: AsRef<Path>>(self, name: &str, path: P) -> io::Result<Self> {
        let path = path.as_ref();
        let (content_type, filename) = mime_filename(path);
        let mut file = File::open(path)?;
        self.stream(&mut file, name, filename, Some(content_type))
    }

    pub fn stream<S: Read>(
        mut self,
        stream: &mut S,
        name: &str,
        filename: Option<&str>,
        content_type: Option<Mime>,
    ) -> io::Result<Self> {
        let content_type = Some(content_type.unwrap_or(mime::APPLICATION_OCTET_STREAM));
        self.write_field_headers(name, filename, content_type)?;
        io::copy(stream, &mut self.inner)?;
        Ok(self)
    }

    fn write_boundary(&mut self) -> io::Result<()> {
        if self.data_written {
            self.inner.write_all(b"\r\n")?;
        }

        write!(self.inner, "--{}\r\n", BOUNDARY_UNIQUE)
    }

    fn write_field_headers(
        &mut self,
        name: &str,
        filename: Option<&str>,
        content_type: Option<Mime>,
    ) -> io::Result<()> {
        self.write_boundary()?;
        if !self.data_written {
            self.data_written = true;
        }
        write!(
            self.inner,
            "Content-Disposition: form-data; name=\"{name}\""
        )?;
        if let Some(filename) = filename {
            write!(self.inner, "; filename=\"{filename}\"")?;
        }
        if let Some(content_type) = content_type {
            write!(self.inner, "\r\nContent-Type: {content_type}")?;
        }
        self.inner.write_all(b"\r\n\r\n")
    }

    pub fn finish(mut self) -> io::Result<(String, Vec<u8>)> {
        if self.data_written {
            self.inner.write_all(b"\r\n")?;
        }

        // always write the closing boundary, even for empty bodies
        write!(self.inner, "--{}--\r\n", BOUNDARY_UNIQUE)?;

        Ok((
            format!("multipart/form-data; boundary=\"{}\"", BOUNDARY_UNIQUE),
            self.inner,
        ))
    }
}

fn mime_filename(path: &Path) -> (Mime, Option<&str>) {
    let content_type = mime_guess::from_path(path);
    let filename = path.file_name().and_then(|filename| filename.to_str());
    (content_type.first_or_octet_stream(), filename)
}
