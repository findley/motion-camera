pub mod echo;
pub mod discord;
pub mod webhook;

use std::time;
use std::path::PathBuf;

pub use echo::EchoHandler;
pub use discord::DiscordHandler;
pub use webhook::WebhookHandler;

pub trait DetectionHandler {
    fn on_detect(&mut self, ts: time::SystemTime, similarity: f64, location: &str);
    fn on_clip_ready(&mut self, ts: time::SystemTime, clip: PathBuf);
    fn name(&self) -> String;
}
