use std::path::PathBuf;
use std::time::{Instant, SystemTime};

use anyhow::Result;

pub struct DiscordHandler {
    bot_token: String,
    dm_user: String,
    dm_channel: Option<String>,
    reply_to: Option<String>,
    last_ping: Option<Instant>,
    ping_count: usize,
}

const PING_COOLDOWN_MS: u128 = 20000;

const EMOJIS: &[&'static str] = &["👀", "🕵️", "🫣", "👁️", "🔎", "🔔"];

impl super::DetectionHandler for DiscordHandler {
    fn name(&self) -> String {
        "DiscordHandler".into()
    }

    fn on_detect(&mut self, _ts: SystemTime, _similarity: f64, location: &str) {
        if self
            .last_ping
            .map(|p| p.elapsed().as_millis() < PING_COOLDOWN_MS)
            .unwrap_or(false)
        {
            log::info!("Not sending discord ping, still waiting on cooldown");
            return;
        }

        let emoji = EMOJIS[self.ping_count % EMOJIS.len()];
        let msg = format!(
            "{} Motion detected at \"{}\". A clip is on the way 📨",
            emoji, location
        );
        match self.send_message(msg) {
            Ok(reply_to) => {
                self.last_ping = Some(Instant::now());
                self.reply_to = reply_to;
                self.ping_count += 1;
            }
            Err(err) => {
                log::error!("Could not send discord message: {}", err)
            }
        }
    }

    fn on_clip_ready(&mut self, _ts: SystemTime, clip: PathBuf) {
        match &self.reply_to {
            Some(reply_to) => {
                match self.post_video(clip, reply_to.into()) {
                    Err(err) => {
                        log::error!("Could not send discord message: {}", err)
                    }
                    _ => {}
                }
                self.reply_to = None;
            }
            None => {
                log::info!("Not posting video clip, because reply_to was not set")
            }
        }
    }
}

impl DiscordHandler {
    pub fn new(bot_token: String, dm_user: String) -> Self {
        Self {
            bot_token,
            dm_user,
            dm_channel: None,
            reply_to: None,
            last_ping: None,
            ping_count: 0,
        }
    }

    fn send_message(&mut self, msg: String) -> Result<Option<String>> {
        log::info!("Sending message");

        let channel_id = self.get_channel()?;
        let url = format!("https://discord.com/api/channels/{channel_id}/messages");
        let request_body = serde_json::json!({
            "content": msg
        });

        let res = ureq::post(&url)
            .set("Authorization", &format!("Bot {}", self.bot_token))
            .set("Content-Type", "application/json")
            .send_string(&request_body.to_string())?;

        let status = res.status();

        match status {
            200 => {
                log::info!("Successfully sent discord message");
                let response_json: serde_json::Value = serde_json::from_str(&res.into_string()?)?;
                Ok(match response_json["id"].as_str() {
                    Some(id) => {
                        log::info!("Message reference ID is: {}", id);
                        Some(id.into())
                    }
                    None => None,
                })
            }
            _ => {
                anyhow::bail!(
                    "Got http response status {} when trying to post message: {}",
                    status,
                    &res.into_string()?
                );
            }
        }
    }

    fn post_video(&mut self, clip: PathBuf, reply_to: String) -> Result<()> {
        log::info!("posting video");

        let channel_id = self.get_channel()?;
        let url = format!("https://discord.com/api/channels/{channel_id}/messages");
        let file_name = clip.file_name().unwrap().to_str().unwrap();

        let request_json_body = serde_json::json!({
            "embeds": [{
                "title": "video clip",
                "video": {
                    "url": format!("attachment://{}", file_name)
                }
            }],
            "attachments": [{
                "id": "0",
                "description": "Motion camera detection clip",
                "filename": file_name
            }],
            "message_reference": { "message_id": reply_to }
        });

        let (content_type, body) = crate::multipart::MultipartBuilder::new()
            .text("payload_json", &request_json_body.to_string())?
            .file("files[0]", clip)?.finish()?;

        let res = ureq::post(&url)
            .set("Authorization", &format!("Bot {}", self.bot_token))
            .set("Content-Type", &content_type)
            .send_bytes(&body)?;


        let status = res.status();

        match status {
            200 => {
                log::info!("Successfully posted video");
                Ok(())
            }
            _ => {
                anyhow::bail!(
                    "Got http response status {} when trying to post video clip: {}",
                    status,
                    res.into_string()?
                );
            }
        }
    }

    fn get_channel(&mut self) -> Result<&str> {
        if self.dm_channel.is_none() {
            log::info!("creating DM channel");

            let request_body = serde_json::json!({
                "recipient_id": self.dm_user
            });

            let res = ureq::post("https://discord.com/api/users/@me/channels")
                .set("Authorization", &format!("Bot {}", self.bot_token))
                .set("Content-Type", "application/json")
                .send_bytes(request_body.to_string().as_bytes())?;

            let status = res.status();
            match status {
                200 => {
                    let response_json: serde_json::Value =
                        serde_json::from_str(&res.into_string()?)?;
                    if let Some(id) = response_json["id"].as_str() {
                        log::info!("DM channel_id is {}", &id);
                        self.dm_channel = Some(id.to_string());
                    } else {
                        anyhow::bail!("No channel_id came back in the response");
                    }
                }
                _ => {
                    log::error!(
                        "Got http response status {} when trying to create DM channel: {}",
                        status,
                        res.into_string()?
                    );
                    anyhow::bail!("Could not create DM channel");
                }
            };
        }

        Ok(self.dm_channel.as_ref().unwrap())
    }
}
