#![allow(unused)]

use std::path::PathBuf;
use std::time::SystemTime;

pub struct WebhookHandler {}

impl super::DetectionHandler for WebhookHandler {
    fn name(&self) -> String {
        "WebhookHandler".into()
    }

    fn on_detect(&mut self, ts: SystemTime, similarity: f64, location: &str) {
        unimplemented!()
    }

    fn on_clip_ready(&mut self, ts: SystemTime, clip: PathBuf) {
        unimplemented!()
    }
}
