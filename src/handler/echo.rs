use std::path::PathBuf;
use std::time::{SystemTime, UNIX_EPOCH};

pub struct EchoHandler {}

impl super::DetectionHandler for EchoHandler {
    fn name(&self) -> String {
        "EchoHandler".into()
    }

    fn on_detect(&mut self, ts: SystemTime, similarity: f64, location: &str) {
        println!(
            "Movement detected at {} @ {} with similarity of {}",
            location,
            ts.duration_since(UNIX_EPOCH).unwrap().as_secs(),
            similarity
        );
    }

    fn on_clip_ready(&mut self, ts: SystemTime, clip: PathBuf) {
        println!(
            "Clip Ready for detection @ {}: {}",
            ts.duration_since(UNIX_EPOCH).unwrap().as_secs(),
            clip.display()
        );
    }
}
