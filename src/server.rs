use touche::http::Result;
use touche::{body::HttpBody, Body, Method, Request, Response, Server, StatusCode};

pub fn start(detecting_tx: crossbeam_channel::Sender<bool>) -> std::io::Result<()> {
    Server::builder()
        .bind("0.0.0.0:4444") // FIXME: Make port configurable
        .serve(
            move |req: Request<Body>| match (req.method(), req.uri().path()) {
                (&Method::PUT, "/detecting") => handle_put_detecting(req, detecting_tx.clone()),
                (_, "/") => Response::builder()
                    .status(StatusCode::OK)
                    .body(Body::from("Hello world!")),

                _ => Response::builder()
                    .status(StatusCode::NOT_FOUND)
                    .body(Body::empty()),
            },
        )
}

fn handle_put_detecting(
    req: Request<Body>,
    detecting_tx: crossbeam_channel::Sender<bool>,
) -> Result<Response<Body>> {
    let body = req.into_body().into_bytes().unwrap_or_default();

    match std::str::from_utf8(&body) {
        Ok(msg) => match is_truthy(msg) {
            Ok(val) => {
                if let Err(_err) = detecting_tx.send(val) {
                    Response::builder()
                        .status(StatusCode::INTERNAL_SERVER_ERROR)
                        .body(Body::from("Could not update detecting value"))
                } else {
                    Response::builder()
                        .status(StatusCode::OK)
                        .body(Body::from(format!(
                            "Detecting is now {}",
                            if val { "enabled" } else { "disabled" }
                        )))
                }
            }
            Err(e) => Response::builder()
                .status(StatusCode::BAD_REQUEST)
                .body(Body::from(e.to_string())),
        },

        Err(err) => Response::builder()
            .status(StatusCode::BAD_REQUEST)
            .body(err.to_string().into()),
    }
}

fn is_truthy(v: &str) -> anyhow::Result<bool> {
    match v.to_lowercase().as_str() {
        "1" | "true" | "yes" | "on" => Ok(true),
        "0" | "false" | "no" | "off" => Ok(false),
        _ => anyhow::bail!("The given value is not a boolean"),
    }
}
