/// We must implement YUVSource for our image data. Unfortunately our camera is sending us
/// YUYV/YUY2 data, which is packed. The openh264 encoder only works with YUV422 planar input, so
/// we must convert our buffer to encode. We also have to downsample the chroma channels, since
/// YUV420 only has one sample per block of 4 pixels, while YUV422 has a sample for every two
/// pixels on a row.
#[allow(dead_code)]
pub struct YUYVSource {
    y: Vec<u8>,
    u: Vec<u8>,
    v: Vec<u8>,
    width: u32,
    height: u32,
}

impl YUYVSource {
    pub fn new(width: u32, height: u32) -> Self {
        let chroma_len = (width * height / 4) as usize;
        Self {
            y: vec![128; (width * height) as usize],
            u: vec![128; chroma_len],
            v: vec![128; chroma_len],
            width,
            height,
        }
    }

    pub fn set_data(&mut self, data: &[u8]) {
        for (i, s) in data.iter().step_by(2).enumerate() {
            self.y[i] = *s;
        }

        let mut chroma_idx = 0;
        let stride = self.width * 2;
        for row_idx in (0..self.height).step_by(2) {
            let row_start_idx = row_idx * stride;
            for col_idx in (0..stride).step_by(4) {
                let u_idx = row_start_idx + col_idx + 1;
                let v_idx = u_idx + 2;

                let next_u_idx = u_idx + stride;
                let next_v_idx = next_u_idx + 2;

                let u_avg = ((data[u_idx as usize] as u16 + data[next_u_idx as usize] as u16) >> 1) as u8;
                self.u[chroma_idx] = u_avg;

                let v_avg = ((data[v_idx as usize] as u16 + data[next_v_idx as usize] as u16) >> 1) as u8;
                self.v[chroma_idx] = v_avg;

                chroma_idx += 1;
            }
        }
    }
}

impl openh264::formats::YUVSource for YUYVSource {
    fn width(&self) -> i32 {
        self.width as i32
    }

    fn height(&self) -> i32 {
        self.height as i32
    }

    fn y(&self) -> &[u8] {
        &self.y
    }

    fn u(&self) -> &[u8] {
        &self.u
    }

    fn v(&self) -> &[u8] {
        &self.v
    }

    fn y_stride(&self) -> i32 {
        self.width as i32
    }

    fn u_stride(&self) -> i32 {
        self.width as i32 / 2
    }

    fn v_stride(&self) -> i32 {
        self.width as i32 / 2
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test_chroma_resampling() {
        #[rustfmt::skip]
        let img = [
            1, 4, 1, 1,  1, 2, 1, 2,
            1, 8, 1, 9,  1, 3, 1, 9,
        //  Y  U  Y  V   Y, U, Y, V
        ];
        let mut s = YUYVSource::new(4, 2);
        s.set_data(&img);
        assert_eq!(2, s.u.len());
        assert_eq!(2, s.v.len());
        assert_eq!(s.u[0], 6);
        assert_eq!(s.v[0], 5);
        assert_eq!(s.u[1], 2);
        assert_eq!(s.v[1], 5);
    }
}
