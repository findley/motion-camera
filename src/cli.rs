use std::path::PathBuf;

use clap::Parser;

use crate::detect::Strategy;

#[derive(Parser)]
#[command(author, version, about, long_about = None)]
pub struct Cli {
    #[arg(short, long, value_name = "DEVICE", help = "Path to video device e.g. /dev/video0")]
    pub cam: Option<PathBuf>,

    #[arg(short, long, value_name = "OUT_DIR", help = "Directory where video clips will be saved")]
    pub out_dir: Option<PathBuf>,

    #[arg(short, long, value_name = "LOCATION", help = "Name for location of camera", default_value = "motion-cam")]
    pub location: String,

    #[arg(short, long, value_enum, default_value = "pixel-diff")]
    pub strategy: Strategy,

    #[arg(short = 't', long, help = "If similarity is below the threshold then detection is triggered [0.0 to 1.0]")]
    pub similarity_thresh: Option<f64>,

    #[arg(short, long, help = "Interval in milliseconds for motion detection", default_value = "500")]
    pub interval: u64,

    /// Turn debugging information on
    #[arg(short, long, action = clap::ArgAction::Count)]
    pub verbose: u8,
}
