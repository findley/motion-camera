use crate::yuv;
use anyhow::Result;
use openh264::encoder::{
    EncodedBitStream, Encoder as H264Encoder, EncoderConfig as H264EncoderConfig,
};
use std::io::Cursor;
use std::path::PathBuf;
use std::time;

pub struct VideoEncoder {
    encoder: H264Encoder,
    // FIXME: The YUV source we create should depend on camera image format.
    yuv_source: yuv::YUYVSource,
    width: u32,
    height: u32,
    rec_start: time::Instant,
    last_frame: time::Instant,
    mp4_writer: Option<mp4::Mp4Writer<Cursor<Vec<u8>>>>,
    mp4_sample: mp4::Mp4Sample,
    track_ready: bool,
}

impl VideoEncoder {
    pub fn new(width: u32, height: u32) -> Self {
        let encoder_config = H264EncoderConfig::new(width, height);
        let encoder = H264Encoder::with_config(encoder_config).unwrap(); // is this unwrap a
                                                                         // programmer error?
        Self {
            encoder,
            yuv_source: yuv::YUYVSource::new(width, height),
            width,
            height,
            rec_start: time::Instant::now(),
            last_frame: time::Instant::now(),
            mp4_writer: None,
            mp4_sample: mp4::Mp4Sample {
                start_time: 0,
                duration: 0,
                rendering_offset: 0,
                is_sync: true,
                bytes: mp4::Bytes::new(),
            },
            track_ready: false,
        }
    }

    pub fn start_recording(&mut self) -> Result<()> {
        self.rec_start = time::Instant::now();
        self.last_frame = time::Instant::now();
        self.track_ready = false;
        let config = mp4::Mp4Config {
            major_brand: str::parse("isom").unwrap(),
            minor_version: 0,
            compatible_brands: vec![
                str::parse("isom").unwrap(),
                str::parse("iso2").unwrap(),
                str::parse("avc1").unwrap(),
                str::parse("mp41").unwrap(),
            ],
            timescale: 1000,
        };
        let buf = Cursor::new(Vec::<u8>::new());
        self.mp4_writer = Some(mp4::Mp4Writer::write_start(buf, &config)?);

        let encoder_config = H264EncoderConfig::new(self.width, self.height);
        self.encoder = H264Encoder::with_config(encoder_config)?;

        Ok(())
    }

    #[allow(dead_code)]
    fn setup_mp4_track(
        &self,
        stream: &EncodedBitStream,
        w: &mut mp4::Mp4Writer<Cursor<Vec<u8>>>,
    ) -> Result<()> {
        let (sps, pps) = scan_for_param_sets(&stream);
        w.add_track(&mp4::TrackConfig {
            track_type: mp4::TrackType::Video,
            timescale: 1000,
            language: "en".to_string(),
            media_conf: mp4::MediaConfig::AvcConfig(mp4::AvcConfig {
                width: self.width as u16,
                height: self.height as u16,
                pic_param_set: pps.ok_or(anyhow::anyhow!(
                    "PPS was not extracted from stream, cann't create mp4 track"
                ))?,
                seq_param_set: sps.ok_or(anyhow::anyhow!(
                    "SPS was not extracted from stream, cann't create mp4 track"
                ))?,
            }),
        })?;

        Ok(())
    }

    pub fn encode_frame(&mut self, buf: &[u8], _metadata: &v4l::buffer::Metadata) -> Result<()> {
        self.yuv_source.set_data(buf);
        let ts = openh264::Timestamp::from_millis(self.rec_start.elapsed().as_millis() as u64);
        log::trace!("encoding h264 frame at timestamp: {}", ts.as_millis());
        let stream = self.encoder.encode_at(&self.yuv_source, ts)?;

        // Can we avoid this, and just move the data directly into the sample?
        let mut bytes = Vec::new();
        stream.write_vec(&mut bytes);

        if let Some(w) = self.mp4_writer.as_mut() {
            if !self.track_ready {
                // self.setup_mp4_track(&stream, w);
                // fuck you rust, let me refactor this block to setup_mp4_track()
                let (sps, pps) = scan_for_param_sets(&stream);
                w.add_track(&mp4::TrackConfig {
                    track_type: mp4::TrackType::Video,
                    timescale: 1000,
                    language: "en".to_string(),
                    media_conf: mp4::MediaConfig::AvcConfig(mp4::AvcConfig {
                        width: self.width as u16,
                        height: self.height as u16,
                        pic_param_set: pps.ok_or(anyhow::anyhow!(
                            "PPS was not extracted from stream, cann't create mp4 track"
                        ))?,
                        seq_param_set: sps.ok_or(anyhow::anyhow!(
                            "SPS was not extracted from stream, cann't create mp4 track"
                        ))?,
                    }),
                })?;
                self.track_ready = true;
            }
            self.mp4_sample.start_time = ts.as_millis();
            self.mp4_sample.duration = self.last_frame.elapsed().as_millis() as u32;
            self.mp4_sample.rendering_offset = 0;
            self.mp4_sample.is_sync = stream.frame_type() == openh264::encoder::FrameType::IDR;
            self.mp4_sample.bytes = mp4::Bytes::copy_from_slice(&bytes);
            w.write_sample(1, &self.mp4_sample)?;
            self.last_frame = time::Instant::now();
            log::trace!("Wrote mp4 sample");
        }

        Ok(())
    }

    pub fn save_file(&mut self, file_path: &PathBuf) {
        log::info!("Saving clip to {}", file_path.display());

        if let Some(mut w) = self.mp4_writer.take() {
            w.write_end().unwrap();
            let mp4_buffer: Vec<u8> = w.into_writer().into_inner();
            std::fs::write(file_path, &mp4_buffer).unwrap();
        }
    }
}

fn scan_for_param_sets(stream: &EncodedBitStream) -> (Option<Vec<u8>>, Option<Vec<u8>>) {
    let mut sps: Option<Vec<u8>> = None;
    let mut pps: Option<Vec<u8>> = None;
    log::trace!("Encoder bitstream has {} layers", stream.num_layers());
    for l in 0..stream.num_layers() {
        let layer = stream.layer(l).unwrap();

        for n in 0..layer.nal_count() {
            let nal = layer.nal_unit(n).unwrap();
            let header_byte = nal[4];
            log::trace!("NAL #{} header byte is {:X} in layer {}", n, header_byte, l);
            if header_byte == 0x67 {
                sps = Some(nal.to_vec());
                log::trace!("SPS found in layer {}: {:X?}", l, nal);
            } else if header_byte == 0x68 {
                pps = Some(nal.to_vec());
                log::trace!("PPS found in layer {}: {:X?}", l, nal);
            }
        }
    }

    (sps, pps)
}
