use crate::DetectionFrame;

pub fn pixel_diff(prev: &DetectionFrame, next: &DetectionFrame) -> f64 {
    let total_diff: u32 = prev
        .pixels()
        .zip(next.pixels())
        .fold(0, |d, (prev_p, next_p)| {
            d + prev_p.0[0].abs_diff(next_p.0[0]) as u32
        });

    1.0 - (total_diff as f64 / (u8::MAX as u32 * next.width() * next.height()) as f64)
}
