use image::io::Reader as ImageReader;
use std::fs;
use std::path::{Path, PathBuf};

use crate::detect::Strategy;
use crate::DetectionFrame;

const ALL_STRATEGIES: &[Strategy] = &[Strategy::PixelDiff];

#[test]
fn test_no_motion() {
    for case in TestCase::list("no-motion") {
        for strat in ALL_STRATEGIES {
            println!("Running test {:?} with strategy {:?}", case.a_path, strat);
            let sim = strat.similarity(&case.a_img(), &case.b_img());
            assert!(sim >= strat.default_thresh());
        }
    }
}

#[test]
fn test_motion() {
    for case in TestCase::list("motion") {
        for strat in ALL_STRATEGIES {
            println!("Running test {:?} with strategy {:?}", case.a_path, strat);
            let sim = strat.similarity(&case.a_img(), &case.b_img());
            assert!(sim < strat.default_thresh());
        }
    }
}

#[test]
#[ignore]
fn test_light_change_pixel_diff() {
    let strat = Strategy::PixelDiff;
    for case in TestCase::list("light-change") {
        println!("Running test {:?} with strategy {:?}", case.a_path, strat);
        let sim = strat.similarity(&case.a_img(), &case.b_img());
        assert!(sim >= strat.default_thresh());
    }
}

#[derive(PartialEq, Eq, PartialOrd, Ord, Debug)]
struct TestCase {
    a_path: PathBuf,
    b_path: PathBuf,
}

impl TestCase {
    fn list(path: &'static str) -> Vec<TestCase> {
        let crate_root_dir = Path::new(env!("CARGO_MANIFEST_DIR"));
        let test_data_dir = crate_root_dir.join("test_images");
        let dir = test_data_dir.join(path);

        let mut res = Vec::new();
        let read_dir = fs::read_dir(&dir)
            .unwrap_or_else(|err| panic!("can't `read_dir` {}: {}", dir.display(), err));
        for file in read_dir {
            let file = file.unwrap();
            let path = file.path();
            if path.file_name().unwrap().to_str().unwrap().contains(".a.") {
                let a_path = path.clone();
                let b_img_name = a_path
                    .file_name()
                    .unwrap()
                    .to_str()
                    .unwrap()
                    .replace(".a.", ".b.");
                let b_path = a_path.with_file_name(b_img_name);
                res.push(TestCase { a_path, b_path });
            }
        }
        if res.is_empty() {
            panic!("No tests found in {}", dir.display());
        }
        res.sort();
        res
    }

    fn a_img(&self) -> DetectionFrame {
        ImageReader::open(&self.a_path)
            .unwrap()
            .decode()
            .unwrap()
            .to_luma8()
    }
    fn b_img(&self) -> DetectionFrame {
        ImageReader::open(&self.b_path)
            .unwrap()
            .decode()
            .unwrap()
            .to_luma8()
    }
}
