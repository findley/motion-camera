mod pixel_diff;
#[cfg(test)]
mod test;

use clap::ValueEnum;

use crate::DetectionFrame;
pub use pixel_diff::pixel_diff;

pub const STRAT_PIXEL_DIFF: &str = "PIXEL_DIFF";
pub const STRAT_BIN_PIXEL_DIFF: &str = "BIN_PIXEL_DIFF";
pub const STRAT_HOUGH_TRANSFORM_DIFF: &str = "HOUGH_TRANSFORM_DIFF";

#[derive(Clone, ValueEnum, Debug)]
pub enum Strategy {
    PixelDiff,
    BinPixelDiff,
    HoughTransformDiff,
}

impl Strategy {
    pub fn similarity(&self, prev: &DetectionFrame, next: &DetectionFrame) -> f64 {
        match self {
            Self::PixelDiff => pixel_diff::pixel_diff(prev, next),
            Self::BinPixelDiff => unimplemented!(),
            Self::HoughTransformDiff => unimplemented!(),
        }
    }

    pub fn default_thresh(&self) -> f64 {
        match self {
            Self::PixelDiff => 0.95,
            Self::BinPixelDiff => unimplemented!(),
            Self::HoughTransformDiff => unimplemented!(),
        }
    }
}

impl From<&str> for Strategy {
    fn from(s: &str) -> Self {
        match s {
            STRAT_PIXEL_DIFF => Self::PixelDiff,
            STRAT_BIN_PIXEL_DIFF => Self::BinPixelDiff,
            STRAT_HOUGH_TRANSFORM_DIFF => Self::HoughTransformDiff,
            _ => Self::PixelDiff,
        }
    }
}
