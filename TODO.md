# TODO

- [x] Install fresh OS on raspberry pie
    - Used rpi-imager, which lets you build settings into the image, like
      username, password, and WIFI settings. This allowed me to image the SD
      card and boot it, then SSH in without needing to mess with an HDMI
      connection. The rpi-imager was suggesting for me to use an older (debian
      bullseye 32 bit) image instead of the bookworm 64-bit image. I decided to
      go ahead with the newer image eventhough the tool didn't suggest it. I
      don't yet have a pi camera module, and I also don't have the right USB
      cables to connect a normal webcam, so I can't test the camera features
      yet. It does appear to have v4l in /dev, so that's a good sign.
    - sudo pacman -S rpi-imager
    - download .img file from <https://www.raspberrypi.com/software/>
    - run rpi-imager and pick the downloaded image
    - be sure to edit settings to put in user and wifi info
    - write the image, then move SD card to pi, and boot
    - add a reserved IP for the mac address in router, so that we can find it
      easily on the network.
    - copy over any necessary SSH keys (don't allow SSH with password)
- [x] Attempt to cross-compile for arm64 and run on raspberry pie
    - Add the rust toolchain for aarch64 `rustup target add aarch64-unknown-linux-gnu`
    - Install aarch64 cross-compiler package `sudo pacman -S aarch64-linux-gnu-gcc`
    - Update the .cargo/config file to use the correct linker
        [target.aarch64-unknown-linux-gnu]
        linker = "aarch64-linux-gnu-gcc"
    - Build: `cargo build --release --target=aarch64-unknown-linux-gnu`
    - Copy the binary to pi and test
- [x] gracefully fail when no camera is found
- [x] Add raw pixel diff strat
    - Iterate the pixels in both images, and diff their chroma values. Add up
      all the total differences, then divide them by the max possible difference
      (#pixels * 255) to get a difference score. 1- this for a similarity score
- [x] organize code (aquistion of a unified Luma image)
    - We'll call these chroma ImageBuffers 'DetectionFrame's
- [x] Make a CLI interface
    - [x] Allow selection of camera
    - [x] Let user specify frame sampling interval
    - clap is pretty nice
- [x] Add more robust testing system
    - I really need to do some reading to understand Path vs PathBuf
- [x] Write debug detection images when ENV var is set
- [x] Add proper logging system
- [x] Record video after motion detection
    - <https://users.rust-lang.org/t/create-mp4-video-from-pictures-in-rust/70850/3>
    - might be an x-compiling nightmare
    - wtf is stride
    - wow YUV encoding is wild. Camera produces YUV422, h264 encoder needs
      YUV420 :/
    - ooooooh, YUV formats are "planar" meaning all the values of a component
      are stored together. I'm used to interleaved components.
    - it looks like getting a video stream to the browser is going to be a lot
      harder <https://stackoverflow.com/questions/52470329/how-to-stream-live-h-264-ip-camera-video-to-browser-bonus-low-bandwidth-and>
    - https://fourcc.org/yuv.php
- [x] Resample chroma YUV422 -> YUV420
    - I made a little diagram with excalidraw then showed it to ChatGPT asking
      it if I had the right idea, and it actually understood it!
    - this was quite tricky to get all the indexes right, but it's working now,
      on my workstation it takes 0.5ms to convert, we'll see how it does in the
      pi.
- [x] Add CLI flag to specify where detection video clips should be saved, if at
  all
    - also refactored the encoder code into a separate module. This module can
      probably handle the mp4 stuff too
- [x] wrap the h264 in mp4 container
    - minimp4 - very simple, but not well tested/maintained?
        - I tested installing this and got a linker error related to clang-sys.
          I think it might be easy to fix, but I didn't mess with it yet
    - mp4 - totally native and complete rust mp4 impl, but the API seems a bit
      complicated, and the docs are very sparse
    - okok, so the h264 encoders (both openh264 and x264) don't provide an easy
      way to get SPS and PPS while encoding, and unfortunately these are needed
      to build the MP4 wrapper. It looks like the only option is to search the
      encoded NAL units for the SPS and PPS vectors. They typically proceed the
      first IDR (keyframe) NAL unit, so we only have to parse the decoder output
      until we find them near the beginning. Technically SPS and PPS can change
      mid stream, but this is very unlikely for a simple and short video clip.
    - NAL units are separated by start codes `0x000001` or `0x00000001`
      depending on the encoder.
    - example:
      ```
      0000 0109 1000 0001 6742 0020 e900 800c
      3200 0001 68ce 3c80 0000 0001 6588 801a
      ```

      Contains 4 NAL units first is just 0x09 (some kinda header). The 167 is
      the SPS and the 168 is the PPS. 165 is the first IDR frame.
    - Managed to write an MP4 that successfully plays, but there are some scary
      looking errors:
    - `ffmpeg -v error -i detection-1704052405066.mp4 -f null -` to check for
      errors
- [x] Build detection handler system
    - should we introduce detection adapters?
        - an adapter could have two methods:
            - trigger, called immediately when motion is detected
            - clip_ready, called once video clip is ready
- [x] Cleanup the garbage code in video_encoder module
- [x] Add a discord handler
    - [x] Send DM on detect
    - [x] Upload clip
    - for now, we just read the discord bot token and DM user from the env.
      These should be moved to a proper config file once we get there.
- [x] Replace reqwest with ureq
  - similar API, but might lighter
  - blocking by default, no tokio dependency
  - does not have multipart support, must be implemented manually
- [x] setup embeded webserver
    - API endpoint to enable/disable detection
    - authentication?
    - https?
    - touche is really nice because you don't have to use tokio (this helps
      keep build times fast, complexity low, and binary small). This little
      embeded server won't be handling much traffic so async rust is way
      overkill.
- [ ] Refactor main event loop
  - maybe pull cam stuff into module
  - need some global state object probably
  - refactor frame processing into a method
  - we can potentially move encoding and detection to separate threads, although
    they're both pretty fast right now and don't risk dropping frames
- [ ] Update discord handler to lookup user ID by name/tag (otherwise you have
  to find your user id which isn't as easy)
- [ ] Move handler triggering to worker thread
    - handlers might be slow, and we don't want to block the video processing
      loop
- [ ] Add cooldown paramater for detection (remove discord handler CD?)
- [ ] Add a webhook handler
    - should we post the whole clip?
    - I guess I'll use reqwest, it doesn't seem like there's any real
      alternative that isn't much lower level.
- [ ] improve enumeration of cameras (might be better to default to /dev/video0)
- [ ] webserver managemnt app frontend
- [ ] HLS video stream from webserver (HARD)
- [ ] give handler access to mp4 buffer directly (avoiding file system)
- [ ] Move camera code to module
- [ ] fix mp4 playback errors (related to SPS probably)
  - Maybe this isn't that high priority, since the mp4 files seem to play, but
    it's driving me crazy
- [ ] test <https://github.com/quadrupleslap/x264> and see if it would be better
  than openh264 (perf, binary size, etc)
    - This one dynamically links to x264, which could still be an option, but it
      means the user will be required to install x264.
    - if the buggy mp4 issues are due to the encoder, then we could look into
      this option instead
- [ ] Add edge/hough diff strat
    - [ ] Add edge detection algo
    - [ ] Add hough transform algo
- [x] Add event loop
    - https://www.reddit.com/r/rust/comments/utoiwy/best_practices_for_local_singlethreaded_event/
    - allows us to handle web server actions
    - we can potentially dispatch encoding or detection work to threads
    - using a crossbeam_channel to allow webserver endpoint to turn detection
      on/off.
- [ ] Add binning diff strat
    - for this we could get the diff of each bin
    - if all bins have a similar diff, then we might decide it was just a
      lighting change. We'd want to look for a local cluster of high diffs.
- [ ] Handle the most common image formats from camera
- [ ] Configuration system (maybe still CLI)
    - [ ] Allow user to specify webhooks
    - [ ] uploading video capture
    - [ ] triggering alerts, etc
- [ ] Add TreeProfiler from rust-analyzer
- [ ] Setup gitlab CI pipelines
- [ ] write a setup/user guide
- [ ] Create a deb package?
