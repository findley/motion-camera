# motion-camera

A CLI app intended to run on raspberry pi (or other other computer with a camera
attached) and detect motion.

## Features

- Multiple strategies for motion detection
- Record short video when motion is detected
- Call configured webhooks when motion is detected
- Support all common camera image formats

## Building

You'll likely want to cross-compile this to aarch64 to run on the raspberry pi.
To do this, you need to install the appropriate cross-compiler package for your
system.

Debian:
```bash
$ sudo apt-get install gcc-aarch64-linux-gnu
```

Arch:
```bash
$ sudo pacman -S aarch64-linux-gnu-gcc
```

You'll also need to install the rust toolchain for the target architecture:
```bash
$ rustup target add aarch64-unknown-linux-gnu
```

Ensure the linker is specified correctly in `.cargo/config`

Build:
```
$ cargo build --release --target=aarch64-unknown-linux-gnu
```

## Testing

A suite of detection pair images can be found in `./test_images`. They're sorted
into directories. All of the image pairs in `./test_images/motion` should
trigger motion detection, while all of the pairs in `./test_images/no-motion`
should not trigger detection. There are test cases in the detect bundle which
will run every image pair against every detection strategy.

Test for `./test_images/light-change` are ignored, because some of the
strategies cannot pass them.

One way to generate test images is to set environment variable
`MOTION_CAM_WRITE_DEBUG_IMAGES=true` before running the program. When motion is
detected, the pair of images that triggered detection will be saved to the
current working directory. Set the similarity threshold very high to write a
detection pair for frames without motion.

## Detection Handlers

There is a `DetectionHandler` trait for generic detection handlers. Potential
handlers could include:

- webhook: multi-purpose
- discord: send direct message
- twilio: send text message
- email: send email
- cloud uploader: upload detection clip to cloud service

### Discord

Set the environment variables:
- `MOTION_CAM_DISCORD_BOT_TOKEN` Create a bot
  <https://discord.com/developers/applications> and get the token
- `MOTION_CAM_DICORD_DM_USER` Currently needs to be the user ID who will recieve
  DMs when motion is detected. This user must be in at least one server with the
  bot. The bot will not send any messages in the server.

When both of these variables are set, the DiscordHandler will be registered.

## Embedded Web Server

A webserver is hosted on port 4444 with an API endpoint at `PUT /detecting`.
Send a boolean value (1, 0, true, false, etc) to turn motion detection on or
off. This can be used to automate turning on detection when you leave your house
for example.

In the future, this server may host a web UI to view or manage settings, and
hopefully to view the live video stream eventually (via HLS or something
similar).

## Logging

This project uses `env_logger` with a default level of `info`. If you want to
see debug logs then set `RUST_LOG=debug`, or just pass the `-v` flag.
